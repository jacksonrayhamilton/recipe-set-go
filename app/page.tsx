"use client";

import { useCallback, useState } from "react";
import { RecipeSuggestions } from "@/components/recipe-suggestions";
import { RecipeResults } from "@/components/recipe-results";

export default function Home() {
  const [query, setQuery] = useState("");
  const handleChange = useCallback((value: string) => {
    setQuery(value);
  }, []);
  return (
    <main className="flex min-h-screen flex-col items-center p-24 gap-4">
      <h1>Recipe, Set, Go!!</h1>
      <div>
        <RecipeSuggestions onChange={handleChange} />
      </div>
      <div className="w-1/2">
        <RecipeResults query={query} />
      </div>
    </main>
  );
}

# Recipe, Set, Go!!

<img alt="Autocompleted Suggestions" src="img/Autocompleted Suggestions.png">

<img alt="Search Results" src="img/Search Results.png">

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

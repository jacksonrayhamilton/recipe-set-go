import queryString from "query-string";

const SPOONACULAR_API_KEY = "f9e1c0abc2584dbda3331f85f26b8977";

type AutocompleteParams = {
  query: string;
  number: number;
};

export type AutocompleteItem = {
  id: number;
  title: string;
  imageType: string;
};

const autocomplete = async ({
  query,
  number,
}: AutocompleteParams): Promise<AutocompleteItem[]> => {
  const resp = await fetch(
    "https://api.spoonacular.com/recipes/autocomplete?" +
      queryString.stringify({ apiKey: SPOONACULAR_API_KEY, query, number }),
  );
  return await resp.json();
};

type ComplexSearchParams = {
  titleMatch?: string;
  number: number;
};

type SearchResponse = {
  offset: number;
  number: number;
  results: SearchResult[];
  totalResults: number;
};

export type SearchResult = {
  id: number;
  title: string;
  image: string;
  imageType: string;
};

const complexSearch = async ({
  titleMatch,
  number,
}: ComplexSearchParams): Promise<SearchResponse> => {
  const resp = await fetch(
    "https://api.spoonacular.com/recipes/complexSearch?" +
      queryString.stringify({
        apiKey: SPOONACULAR_API_KEY,
        titleMatch,
        number,
      }),
  );
  return await resp.json();
};

export { autocomplete, complexSearch };

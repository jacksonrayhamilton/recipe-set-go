import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import * as RecipeAPI from "@/lib/recipe-api";
import type { SearchResult } from "@/lib/recipe-api";

type RecipeResultsProps = {
  query: string;
};

const RecipeResults = ({ query }: RecipeResultsProps) => {
  const lastResultsRequest = useRef<object>();
  const [results, setResults] = useState<SearchResult[]>([]);

  useEffect(() => {
    const resultsRequest = {};
    lastResultsRequest.current = resultsRequest;
    (async () => {
      if (!query) setResults([]);
      else {
        const results = await RecipeAPI.complexSearch({
          titleMatch: query,
          number: 5,
        });
        if (lastResultsRequest.current === resultsRequest) {
          setResults(results.results);
        }
      }
    })();
  }, [query]);

  return !query ? null : results.length === 0 ? (
    <p className="text-center">No Results</p>
  ) : (
    <ul className="flex flex-col gap-2">
      {results.map((result) => (
        <li
          key={result.id}
          className="rounded-lg px-5 py-4 bg-slate-100 flex gap-4 items-center"
        >
          <Image
            alt={result.title}
            src={result.image}
            width={96}
            height={71}
            className="w-24"
          />
          <span>{result.title}</span>
        </li>
      ))}
    </ul>
  );
};

export { RecipeResults };

import { useCallback, useRef, useState } from "react";
import * as RecipeAPI from "@/lib/recipe-api";
import type { AutocompleteItem } from "@/lib/recipe-api";
import { SuggestionInput } from "@/components/ui/suggestion-input";

type RecipeSuggestionsProps = {
  onChange: (value: string) => unknown;
};

const RecipeSuggestions = ({ onChange }: RecipeSuggestionsProps) => {
  const lastAutocompleteRequest = useRef<object>();
  const [suggestions, setSuggestions] = useState<AutocompleteItem[]>([]);
  const searchForSuggestions = useCallback(async (query: string) => {
    const autocompleteRequest = {};
    lastAutocompleteRequest.current = autocompleteRequest;
    if (!query) setSuggestions([]);
    else {
      const suggestions = await RecipeAPI.autocomplete({ query, number: 5 });
      if (lastAutocompleteRequest.current === autocompleteRequest) {
        setSuggestions(suggestions);
      }
    }
  }, []);
  return (
    <SuggestionInput
      className="w-64"
      placeholder="Search for a recipe…"
      suggestions={suggestions}
      onInput={searchForSuggestions}
      onChange={onChange}
    />
  );
};

export { RecipeSuggestions };

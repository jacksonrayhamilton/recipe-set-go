import { useCallback, useEffect, useState } from "react";
import { Input } from "@/components/ui/input";
import {
  Popover,
  PopoverAnchor,
  PopoverContent,
} from "@/components/ui/popover";

type Suggestion = {
  id: number;
  title: string;
};

type SuggestionInputProps = Omit<
  React.ComponentProps<typeof Input>,
  "onChange" | "onInput"
> & {
  suggestions: Suggestion[];
  onInput?: (value: string) => unknown;
  onChange?: (value: string) => unknown;
};

const SuggestionInput = ({
  suggestions = [],
  onInput,
  onChange,
  ...inputProps
}: SuggestionInputProps) => {
  const [showSuggestions, setShowSuggestions] = useState(
    suggestions.length > 0,
  );
  // Cache the last non-empty value so when the popover closes due to having no
  // suggestions, it doesn't momentarily have no items in it while it fades.
  const [suggestionsToRender, setSuggestionsToRender] = useState(suggestions);
  useEffect(() => {
    if (suggestions.length > 0) {
      setShowSuggestions(true);
      setSuggestionsToRender(suggestions);
    } else {
      setShowSuggestions(false);
    }
  }, [suggestions]);
  const [inputValue, setInputValue] = useState("");
  const handleInput = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setInputValue(event.currentTarget.value);
      onInput?.(event.currentTarget.value);
      onChange?.(event.currentTarget.value);
    },
    [onInput, onChange],
  );
  const handleInteractOutsidePopover = useCallback(() => {
    setShowSuggestions(false);
  }, []);
  return (
    <Popover open={showSuggestions}>
      <PopoverAnchor>
        <Input value={inputValue} onChange={handleInput} {...inputProps} />
      </PopoverAnchor>
      <PopoverContent
        onOpenAutoFocus={(e) => e.preventDefault()}
        onInteractOutside={handleInteractOutsidePopover}
      >
        {suggestionsToRender.length && (
          <ul>
            {suggestionsToRender.map((suggestion) => (
              <li
                key={suggestion.id}
                className="px-2 py-1 rounded cursor-pointer hover:bg-slate-100"
                onClick={() => {
                  setInputValue(suggestion.title);
                  onChange?.(suggestion.title);
                  setShowSuggestions(false);
                }}
              >
                {suggestion.title}
              </li>
            ))}
          </ul>
        )}
      </PopoverContent>
    </Popover>
  );
};

export { SuggestionInput };
